#ifndef APP_H_INCLUDED
#define APP_H_INCLUDED

#include <dirent.h>
#include "init.h"
#include "graphics.h"
#include "texture.h"
#include "font.h"
#include "events.h"
#include "gui.h"
#include "menu.h"
#include "editor.h"
#include "tileset.h"
#include "tilemap.h"
#include "file.h"

class App {
public:
        App();
        ~App();
        void Init();
        void Update();
        void Draw();
        void Quit();
        Events Event;
        Button btn_01;
        EditBox edt_01;
        BitmapFont text;
        BitmapFont text2;
        BitmapFont dbg_tile;
        BitmapFont dbg_select;
        BitmapFont dbg_texture;
        Menu menu;
        Editor editor;
        TileSet tileset;
        TileMap tilemap;
        TILEMAP tmap;


private:
        SDL_DisplayMode current;
        DIR *dir;
        const char* dirname;
        struct dirent *directory;




};

#endif // APP_H_INCLUDED
