#include "texture.h"
#include "graphics.h"
#include <iostream>

TextureManager* TextureManager::instance_ = 0;

//Texture wrapper
//constructor
Texture::Texture(){
    std::cout<< "Texture created" <<std::endl;
    texture = NULL;
}
//constructor
Texture::Texture(std::string file){
    filename = file.c_str();
    load(file);
}


//Destructor
Texture::~Texture(){
    if(texture!=NULL)
    {
        std::cout<< "Texture deleted " << filename <<std::endl;
        SDL_DestroyTexture(texture);
        texture = NULL;
    }

}

Texture::Texture(const Texture &cpy)
{
std::cout<< "Texture copied" <<filename<<std::endl;

}

//Texture load
bool Texture::load(std::string file){
    SDL_Surface* surface = IMG_Load(file.c_str());
    filename = file.c_str();
    if(!surface){
        std::cerr<< "Error file "<<filename<< " not found "<<std::endl;
        SDL_FreeSurface(surface);
        return false;
    }
    texture = SDL_CreateTextureFromSurface(GraphicsDevice::instance().m_render,surface);
    SDL_FreeSurface(surface);
    SDL_QueryTexture(texture,NULL,NULL,&w,&h);
    return true;
}

int TextureManager::getTextureWidth(std::string textureID)
{
return tex[textureID]->getWidth();
}

int TextureManager::getTextureHeight(std::string textureID)
{
return tex[textureID]->getHeight();
}

SDL_Texture* Texture::get()
{
if(texture!=NULL)
 return texture;
else
return 0;

}


//
bool TextureManager::Load(std::string filename, std::string ID)
{
    Texture *newTex = new Texture;
    if(!newTex->load(filename))
    {
        std::cerr<< "Unable to load texture "<< filename <<std::endl;
        return false;
    }
    tex.insert(std::make_pair(ID,newTex));
    return true;
}



//Gettexture from bank
SDL_Texture* TextureManager::getTexture(std::string textureID)
{
    return  tex[textureID]->get();
}

void TextureManager::unloadAll()
{
   std::unordered_map<std::string,Texture*>::iterator it;
   for(it = tex.begin(); it!= tex.end();it++)
   {
   delete(it->second);
   }
   tex.clear();

}
