#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include "gui.h"

class Menu{
public:
        Menu();
        ~Menu();
        void Init();
        void Update();
        void Draw();
        Window win_about;
        Window win_open;
        Window win_save;
        Window win_new;
        Button btn_new;
        Button btn_open;
        Button btn_save;
        Button btn_about;
        BitmapFont txt_debug;

private:
        BitmapFont font;
};
#endif // MENU_H_INCLUDED
