#ifndef TILEMAP_H_INCLUDED
#define TILEMAP_H_INCLUDED
#include "graphics.h"
#include "texture.h"
#include "tileset.h"

class TileMap{
public:
         TileMap();
         ~TileMap();
         int x, y;
         int w, h;
         int tilew, tileh;
         int total;
         void getTileSet(TileSet ts);
         void setTexture(std::string textureID, int offset);
         void setTileMap(int w, int h, int tilew, int tileh);
         int *matrix;
         void draw();
         int col;
private:
        int tilesetw, tileseth;

        SDL_Rect src, dst;
        int offset;
        std::string textureID;
};

class TILEMAP{
public:
        //TILEMAP(){};
        //~TILEMAP(){};
        char magic[4];
        int tile;
        int width;
        int height;
        int total;
        int *matrix;

};

#endif // TILEMAP_H_INCLUDED
