#ifndef EVENTS_H_INCLUDED
#define EVENTS_H_INCLUDED

#include <SDL2/SDL_events.h>
#include <string>


class Events{
public:
      void Init();
      void PollEvents();
      bool getState(){return isRunning;}

      static std::string text;

      static bool KeyA;
      static bool KeyD;
      static bool KeyW;
      static bool KeyS;

private:
     SDL_Event event;
     bool isRunning;

};

#endif // EVENTS_H_INCLUDED
