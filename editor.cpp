#include "editor.h"
#include "events.h"
#include "texture.h"

//Constructor
Editor::Editor(){
isActive = true;
mapViewPort.x=0;
mapViewPort.y=0;
mapViewPort.w=800;
mapViewPort.h=600;

selector.w=16;
selector.h=16;

tileh=16;
tilew=16;
}


//Destructor
Editor::~Editor(){
}


//Initilize editor
void Editor::init(){
tilemap.setTileMap(40,30,16,16);
tilemap.setTexture("tileset",1);

}


//Update editor every frame
void Editor::update(){
    if(isActive){
    camera.moveCamera();
    SDL_GetMouseState(&mx, &my);
    r_mx = mx-5;
    r_my = my-35;
    x_tile=((r_mx-camera.x)/tilew);
    y_tile=((r_my-camera.y)/tileh);
    selector.x=((mx/tilew)*tilew);
    selector.y=((my/tileh)*tileh)-32;
    tilemap.x=camera.x;
    tilemap.y=camera.y;

    if((mx>mapViewPort.x) && (mx<mapViewPort.x+mapViewPort.w) && (my>mapViewPort.y)&&(my<mapViewPort.y+mapViewPort.h))
    if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)){
     tileSelected = x_tile+(y_tile*tilemap.w);
     tilemap.matrix[tileSelected]=tileID;
    }

    }

}


//Render
void Editor::draw(){
    //SDL_RenderSetViewport(GraphicsDevice::instance().m_render,&mapViewPort);


    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,255,255,255,255);
    SDL_RenderFillRect(GraphicsDevice::instance().m_render,&mapViewPort);

    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,61,61,61,255);
    SDL_RenderDrawRect(GraphicsDevice::instance().m_render,&mapViewPort);

    camera.draw();
    tilemap.draw();
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,255,0,0,255);
   // SDL_RenderDrawRect(GraphicsDevice::instance().m_render,&selector);


//SDL_RenderCopy(GraphicsDevice::instance().m_render, TextureManager::instance().getTexture("guide"),NULL,&mapViewPort);
}
