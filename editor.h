#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED
#include "graphics.h"
#include "camera.h"
#include "tilemap.h"
#include "file.h"

class Editor{
public:
      Editor();
      ~Editor();
      void init();
      void update();
      void draw();
      int mx, my;
      int r_mx, r_my;
      int mapx, mapy;
      int mapw, maph;
      int x_tile;
      int y_tile;
      int tileSelected;
      int tileID;
      bool isActive;
      SDL_Rect selector;
      Camera camera;
      TileMap tilemap;
      TILEMAP tmap;

private:
      int tilew, tileh;
      SDL_Rect mapViewPort;



};

#endif // EDITOR_H_INCLUDED
