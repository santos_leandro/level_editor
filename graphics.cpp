#include "graphics.h"
#include <iostream>


//Static main render , main window
GraphicsDevice* GraphicsDevice::instance_ = 0;



//Create main window
void GraphicsDevice::createWindow(const char* title,int width, int height)
{
    m_window = SDL_CreateWindow(title,SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, width,height,SDL_WINDOW_MAXIMIZED);
    if(!m_window)
    {
        std::cerr<< "Error creating Window" <<std::endl;
        SDL_DestroyWindow(m_window);
        SDL_Quit();
    }
    else std::cout<< "Window created " <<std::endl;

}

//Create  main render
void GraphicsDevice::createRenderer(Uint32 flags)
{

  m_render = SDL_CreateRenderer(m_window, -1, flags);
  if(!m_render)
  {
  std::cerr<< "Error creating Renderer" <<std::endl;
  SDL_DestroyRenderer(m_render);
  SDL_DestroyWindow(m_window);
  SDL_Quit();
  }
  else std::cout<< "Render created" <<std::endl;

}

//clear all
void GraphicsDevice::quit()
{
  SDL_DestroyRenderer(m_render);
  SDL_DestroyWindow(m_window);
}
