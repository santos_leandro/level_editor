#include "camera.h"
#include "events.h"


//Constctor
Camera::Camera(){
x=0; y=0;
step=16;
}

//destructor
Camera::~Camera(){
}

//ASWD to move camera
void Camera::moveCamera(){
    if(Events::KeyA){
        x_direction=-1;
         SDL_Delay(100);}
        else if(Events::KeyD){
           x_direction=1;
            SDL_Delay(100);}
            else x_direction=0;

    if(Events::KeyS){
        y_direction=1;
        SDL_Delay(100);}
        else if(Events::KeyW){
            y_direction=-1;
            SDL_Delay(100);}
             else y_direction=0;
    x+=step*x_direction;
    y+=step*y_direction;

    camera_rect.x=x;
    camera_rect.y=y;
    camera_rect.w=w;
    camera_rect.h=h;
}

//Draw camera
void Camera::draw(){
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render, 190,190,190,255);
    SDL_RenderFillRect(GraphicsDevice::instance().m_render,&camera_rect);

}
