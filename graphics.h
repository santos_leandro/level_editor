#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_image.h>
#include <iostream>

class GraphicsDevice
{
public:

     ~GraphicsDevice(){std::cout<<"End graphics"<<std::endl;}
     static GraphicsDevice& instance()
     {
      if(!instance_)
       instance_ = new GraphicsDevice;
      return *instance_;
     }

     void   createWindow    (const char* Title,int width, int height);
     void   createRenderer  (Uint32 flags);
     void   quit();
     SDL_Renderer*  m_render;
     SDL_Window*    m_window;
private:
     GraphicsDevice (){}
     static GraphicsDevice *instance_;

};


#endif // GRAPHICS_H_INCLUDED
