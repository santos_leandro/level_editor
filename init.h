#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

#include <SDL2/SDL.h>

bool init_all();

bool init_graphics();

bool init_audio();



#endif // INIT_H_INCLUDED
