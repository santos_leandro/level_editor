#ifndef TILESET_H_INCLUDED
#define TILESET_H_INCLUDED
#include "graphics.h"
#include "texture.h"

class TileSet{
public:
        TileSet();
        ~TileSet();
        int x, y;
        int tilew, tileh;
        void setTile(int w, int h){tilew=w; tileh=h;}
        void getTexture(std::string textureID, int offset);
        void update();
        void draw();
        int tileID;
        int idx, idy;
private:
        int offset;
        int mx, my;
        SDL_Rect view_texture;
        std::string textureID;
        int w, h;
        int row, col;

};


#endif // TILESET_H_INCLUDED
