#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED
#include "graphics.h"

class Camera{
public:
        Camera();
        ~Camera();
        int x, y;
        int w, h;
        void moveCamera();
        void setStep(int step){this->step = step;}
        void setCameraSize(int w, int h){this->w=w;this->h=h;}
        void draw();
private:
        SDL_Rect camera_rect;
        int step;
        int x_direction;
        int y_direction;

};

#endif // CAMERA_H_INCLUDED
