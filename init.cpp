//
//  Initilize SDL2
//
//
//

#include "init.h"
#include <iostream>

static SDL_Joystick* g_joy1 = NULL;

bool init_all()
{
  if(SDL_Init(SDL_INIT_EVERYTHING)!=0)
  {
  std::cerr<< "Error Init" <<std::endl;
  SDL_Quit();
  return false;
  }
  else std::cout<<"SDL Init() ok"<<std::endl;
  if (SDL_NumJoysticks()<0)
  {
  std::cerr<< "Joystick not found "<<std::endl;
  return false;
  }
  else std::cout<<"Joystick found "<<std::endl;

  g_joy1 = SDL_JoystickOpen(0);

  return true;
}
