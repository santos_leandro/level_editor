#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#include "texture.h"
#include "font.h"
#include "graphics.h"
#include <unordered_map>


class Button{
public:
     Button();
     void GetTexture(std::string texture);
     void setPosition(int x_pos, int y_pos){x = x_pos; y = y_pos;}
     bool OnClick();
     void Draw();
     void GetFont(BitmapFont font_){font = font_;}
     void SetText(std::string text){font.setText(text,x+12,y+6);}
private:
     BitmapFont font;
     std::string textureID;
     int x, y;
     int w, h;
};


class EditBox{
public:
     EditBox();
     void GetTexture(std::string texture);
     void setPosition(int x_pos, int y_pos){x = x_pos; y = y_pos;}
     bool OnClick();
     void Draw();
     void GetInput();
     void GetFont(BitmapFont font_){font = font_;}
     void SetText(std::string text){font.setText(text,x+5,y+3);}
     std::string GetText(){return font.getText();}
     bool isActive;
private:
     BitmapFont font;
     std::string textureID;
     int x, y;
     int w, h;
};

class Window{
public:
      Window();
      ~Window();
      void SetWindow(int W, int H, int X, int Y);
      void CreateWindow(int X, int Y, int W, int H);
      void AddButton(std::string name, int x, int y, BitmapFont font);
      void AddEditBox(std::string name, int x, int y, BitmapFont font);
      void AddText(std::string name, std::string strText, int x, int y);
      void Update();
      void Draw();
      bool isActive;
      std::unordered_map<std::string, BitmapFont*> text;
      std::unordered_map<std::string, Button*> button;
      std::unordered_map<std::string, EditBox*> editbox;

private:
      std::string temp;
      SDL_Rect viewport;
      SDL_Rect dst;
      int w, h, x, y;
};

#endif // GUI_H_INCLUDED
