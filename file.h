#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include "tilemap.h"

void loadMap(const char* filename, TILEMAP &tilemap);
void saveMap(const char* filename, TILEMAP &tilemap);


#endif // FILE_H_INCLUDED
