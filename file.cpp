#include "file.h"
#include <cstring>

void loadMap(const char* filename, TILEMAP &tilemap){
    FILE *file;
    file = fopen(filename,"rb");
    fread(&tilemap,sizeof(tilemap),1,file);

    tilemap.matrix = new int[tilemap.total];
    fread(tilemap.matrix,sizeof(int),tilemap.total,file);
    fclose(file);
}

void saveMap(const char* filename, TILEMAP &tilemap){
FILE *file;
file = fopen(filename,"wb");
fwrite(&tilemap,sizeof(tilemap),1,file);
fwrite(tilemap.matrix,sizeof(int),tilemap.total,file);
fclose(file);
}
