#include "events.h"

std::string Events::text ="";

   bool Events::KeyA=false;
   bool Events::KeyD=false;
   bool Events::KeyW=false;
   bool Events::KeyS=false;

//initialze isRunnig var
void Events::Init(){
    isRunning = true;
}


//Poll events, get key pressed, and get text input
void Events::PollEvents(){

    while(SDL_PollEvent(&event))
    {
        if(event.type == SDL_QUIT)
            isRunning=false;
        if(event.type == SDL_TEXTINPUT)
            text += event.text.text;

        if(event.type == SDL_KEYDOWN)
            switch(event.key.keysym.sym)
            {
             case SDLK_BACKSPACE:
             if(text.size()!=0)
             text.pop_back();
             break;
             case SDLK_ESCAPE:
             isRunning=false;
             break;
             case SDLK_a:
             KeyA=true;
             break;
             case SDLK_d:
             KeyD=true;
             break;
             case SDLK_w:
             KeyW=true;
             break;
             case SDLK_s:
             KeyS=true;
             break;

            }


        if(event.type == SDL_KEYUP)
            switch(event.key.keysym.sym)
            {
             case SDLK_a:
             KeyA=false;
             break;
             case SDLK_d:
             KeyD=false;
             break;
             case SDLK_w:
             KeyW=false;
             break;
             case SDLK_s:
             KeyS=false;
             break;
            }


    }

}
