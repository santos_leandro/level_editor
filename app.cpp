#include "app.h"


SDL_Rect view_map;
SDL_Rect view_menu;


App::App(){
}

App::~App(){

}


//Init start sdl, events, setup graphics, load textures, load menu and texts
void App::Init(){

    init_all();
    Event.Init();

    SDL_GetDesktopDisplayMode(0,&current);
    GraphicsDevice::instance().createWindow("App 01", current.w, current.h);
    SDL_SetWindowResizable(GraphicsDevice::instance().m_window,SDL_TRUE);
    //GraphicsDevice::instance().createRenderer(SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    GraphicsDevice::instance().createRenderer(SDL_RENDERER_ACCELERATED);



    TextureManager::instance().Load("textures/user/tileset-mario.png","tileset");

    TextureManager::instance().Load("textures/system/font_black.png","font01");

    TextureManager::instance().Load("textures/system/editbox_16.png","editbox");

    TextureManager::instance().Load("textures/system/btn_base.png","button");

    TextureManager::instance().Load("textures/system/base.png","base");

    TextureManager::instance().Load("textures/system/guide_lines.png","guide");


    menu.Init();


    text.setFont("font01",7,9);
    text.setSize(1);

    text2.setFont("font01",7,9);
    text2.setSize(1);

    dbg_tile.setFont("font01",7,9);
    dbg_tile.setSize(1);

    dbg_select.setFont("font01",7,9);
    dbg_select.setSize(1);

    dbg_texture.setFont("font01",7,9);
    dbg_texture.setSize(1);

    btn_01.GetTexture("button");
    btn_01.setPosition(900,700);
    btn_01.GetFont(text);
    btn_01.SetText("  Ok");

    edt_01.GetTexture("editbox");
    edt_01.setPosition(950,150);
    edt_01.GetFont(text);
    edt_01.SetText("Type here");

    view_map.x=5;
    view_map.y=35;
    view_map.w=800;
    view_map.h=600;

    view_menu.x=0;
    view_menu.y=0;
    view_menu.w=current.w;
    view_menu.h=current.h;

    editor.camera.w=640;
    editor.camera.h=480;


    tileset.tileh=16;
    tileset.tilew=16;
    tileset.getTexture("tileset",1);


    tilemap.setTexture("tileset",1);
    tilemap.setTileMap(40,30,16,16);

    editor.init();

    tmap.matrix = new int[tilemap.total];

}


//Poll events, update menu, tileset, editor
void App::Update(){

    Event.PollEvents();

    menu.Update();

    tileset.update();

    editor.tileID=tileset.tileID;

    if(!menu.win_save.isActive&&!menu.win_open.isActive)
    editor.update();






    std::string mouse = "absolute mouse x = "+std::to_string(editor.mx)+" mouse y = "+std::to_string(editor.my);
    std::string mouse_relative = "relative mouse x = "+std::to_string(editor.r_mx)+"mouse y = "+std::to_string(editor.r_my);
    std::string tile_select ="tile x= "+std::to_string(editor.x_tile)+" tile y= "+std::to_string(editor.y_tile)+" col "+std::to_string(editor.tilemap.col);
    std::string str_select = "camera x "+std::to_string(editor.camera.x)+" camera y "+std::to_string(editor.camera.y);
    //std::string str_texture = "texture x "+std::to_string(tileset.idx)+" texture y "+std::to_string(tileset.idy)+" tile id "+std::to_string(tileset.tileID);
    std::string str_texture = " active save "+std::to_string(menu.win_save.isActive);
    text.setText(mouse,600,650);
    text2.setText(mouse_relative,600,670);
    dbg_tile.setText(tile_select,600,690);
    dbg_select.setText(str_select,600,720);
    dbg_texture.setText(str_texture,600,760);



    if(menu.win_open.button["ok"]->OnClick()&&menu.win_open.isActive){
        loadMap("map01.bin",tmap);
        for(int i=0;i<tmap.total;i++){
        editor.tilemap.matrix[i]=tmap.matrix[i];
        }
    }

    if(menu.win_save.button["ok"]->OnClick()&&menu.win_save.isActive){

        strcpy(tmap.magic,"MAP");
        tmap.tile = tilemap.tilew;
        tmap.width = tilemap.w;
        tmap.height = tilemap.h;
        tmap.total = tilemap.total;
        for(int i=0;i<tmap.total;i++){
        tmap.matrix[i]=editor.tilemap.matrix[i];
        }
        saveMap("map01.bin",tmap);

        //delete[] tmap.matrix;
    }

    if(menu.win_save.button["cancel"]->OnClick()&&menu.win_save.isActive){
    menu.win_save.isActive=false;
    }



}

//Render tileset menu editor and texts
void App::Draw(){


    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render, 180,180,180,255);
    SDL_RenderClear(GraphicsDevice::instance().m_render);

    tileset.draw();

    SDL_RenderSetViewport(GraphicsDevice::instance().m_render,&view_map);
    editor.draw();



    SDL_RenderSetViewport(GraphicsDevice::instance().m_render,&view_menu);
    menu.Draw();

    text.draw();
    text2.draw();
    dbg_tile.draw();
    dbg_select.draw();
    dbg_texture.draw();

    SDL_RenderPresent(GraphicsDevice::instance().m_render);

}
