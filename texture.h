#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <SDL2/SDL_image.h>
#include <vector>
#include <map>
#include <unordered_map>


class Texture
{
public:
 Texture();
 Texture(std::string file);
 Texture(const Texture &cpy);
 ~Texture();
 bool load(std::string file);
 int getWidth(){return w;}
 int getHeight(){return h;}
 SDL_Texture* get();
private:
 SDL_Texture *texture;
 std::string filename;
 unsigned int id;
 int w, h;
};


class TextureManager
{
public:
        static TextureManager &instance()
        {
        if(!instance_)
        {
            instance_ = new TextureManager;
        }
        return *instance_;
        }
        bool Load(std::string filename, std::string ID);
        SDL_Texture* getTexture(std::string textureID);
        int getTextureWidth(std::string textureID);
        int getTextureHeight(std::string textureID);
        void unload(std::string filename);
        void unloadAll();


private:
        TextureManager(){}
        static TextureManager *instance_;
        std::unordered_map <std::string, Texture*> tex;
};

#endif // TEXTURE_H_INCLUDED
