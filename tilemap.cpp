#include "tilemap.h"

TileMap::TileMap(){
matrix=NULL;
}

TileMap::~TileMap(){
if(matrix!=NULL)
delete[] matrix;
}



void TileMap::setTexture(std::string textureID, int offset){
this->textureID = textureID; this->offset=offset;
SDL_QueryTexture(TextureManager::instance().getTexture(textureID),NULL,NULL,&tilesetw,&tileseth);
col=tilesetw/(tilew+offset);

}
void TileMap::setTileMap(int w, int h, int tilew, int tileh){
this->w=w;
this->h=h;
this->tilew=tilew;
this->tileh=tileh;
total = w*h;
matrix = new int[total];
for(int i=0;i<total;i++)
    {
     matrix[i]=24;

    }

}

void TileMap::draw(){
    src.x=0;
    src.y=0;
    src.w =  tilew;
    src.h =  tileh;
    dst.w =  tilew;
    dst.h =  tileh;
    dst.y =  x;
    dst.x =  y;

    for(int i =0; i<total; i++)
    {
        src.x=(matrix[i]%col) * (tilew+offset);
        src.y=(matrix[i]/col) * (tileh+offset);

        dst.x = ((i%w)*tilew)+x;
        dst.y = ((i/w)*tileh)+y;

        if(matrix[i]!=0)
           SDL_RenderCopy(GraphicsDevice::instance().m_render,TextureManager::instance().getTexture(textureID),&src,&dst);
    }


}
