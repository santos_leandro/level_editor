#include "font.h"
#include "texture.h"
#include "graphics.h"

BitmapFont::BitmapFont(){
fontSize =1;

}

BitmapFont::~BitmapFont(){

}

void BitmapFont::setFont(std::string textureId, int fontWidth, int fontHeight){
    w = fontWidth;
    h = fontHeight;
    textureID = textureId;
    int texWidth = TextureManager::instance().getTextureWidth(textureID);
    col = texWidth/fontWidth;
}

void BitmapFont::setText(std::string text, int x_pos, int y_pos){
    x = x_pos;
    y = y_pos;
    text_ =text;
}

void BitmapFont::draw(){
    int ascii;
    SDL_Rect src;
    SDL_Rect dst;
    src.x=0;
    src.y=0;
    src.w=w;
    src.h=h;
    dst.x=x;
    dst.y=y;
    dst.w=w*fontSize;
    dst.h=h*fontSize;

    for(Uint16 i=0;i<text_.length();i++){

            ascii = (unsigned char)text_[i];
            src.x=((ascii-32)%col)*src.w;
            src.y=((ascii-32)/col)*src.h;
            if(ascii==10){
            dst.x=x-w;
            dst.y+=h*fontSize;

            }
            SDL_RenderCopy(GraphicsDevice::instance().m_render,TextureManager::instance().getTexture(textureID),&src,&dst);
            dst.x+=src.w*fontSize;
        }
}
