#include "gui.h"
#include <iostream>
#include "events.h"

Button::Button(){
textureID = "button";
}

void Button::GetTexture(std::string texture)
{
textureID = texture;
w = TextureManager::instance().getTextureWidth(textureID);
h = TextureManager::instance().getTextureHeight(textureID);

}

bool Button::OnClick(){
 int mx, my;
 if(SDL_GetMouseState(&mx, &my)&SDL_BUTTON(SDL_BUTTON_LEFT))
 {
    if((mx>x)&&(mx<x+w)&&(my>y)&&(my<y+h))
     return true;
 }
 return false;
}

void Button::Draw(){
    SDL_Rect dst;
    dst.x=x;
    dst.y=y;
    dst.w=w;
    dst.h=h;

    SDL_RenderCopy(GraphicsDevice::instance().m_render,TextureManager::instance().getTexture(textureID),NULL,&dst);
    font.draw();
}


EditBox::EditBox(){
    textureID = "editbox";
    isActive = false;
}

void EditBox::GetTexture(std::string texture){
    textureID = texture;
    w = TextureManager::instance().getTextureWidth(textureID);
    h = TextureManager::instance().getTextureHeight(textureID);
}

bool EditBox::OnClick(){
 int mx, my;
 SDL_GetMouseState(&mx, &my);
 if((SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(SDL_BUTTON_LEFT))&&((mx>x)&&(mx<x+w)&&(my>y)&&(my<y+h)))
 {
    Events::text = font.getText();
    return true;
 }
    return false;
}

void EditBox::GetInput(){
if(isActive){
 SetText(Events::text);
 }
}

void EditBox::Draw()
{
    SDL_Rect dst;
    dst.x=x;
    dst.y=y;
    dst.w=w;
    dst.h=h;

    SDL_RenderCopy(GraphicsDevice::instance().m_render,TextureManager::instance().getTexture(textureID),NULL,&dst);
    font.draw();
}


Window::Window(){
    isActive=false;
    temp="";
}

Window::~Window(){
    std::unordered_map<std::string, Button*>::iterator bt_it;
        for(bt_it = button.begin(); bt_it!=button.end();bt_it++)
            delete bt_it->second;

    std::unordered_map<std::string, EditBox*>::iterator ed_it;
        for(ed_it = editbox.begin(); ed_it!=editbox.end();ed_it++)
            delete ed_it->second;

    std::unordered_map<std::string, BitmapFont*>::iterator txt_it;
        for(txt_it = text.begin(); txt_it!=text.end();txt_it++)
            delete txt_it->second;

        button.clear();
        editbox.clear();
        text.clear();
}

void Window::SetWindow(int x, int y, int w, int h){
    this->w=w; this->h=h; this->x=x; this->y=y;
}



void Window::CreateWindow(int x, int y, int w, int h){
    this->w=w; this->h=h; this->x=x; this->y=y;
}


void Window::AddButton(std::string name, int X, int Y, BitmapFont font){
    Button *btn = new Button;
    btn->setPosition(x+X,y+Y);
    btn->GetTexture("button");
    btn->GetFont(font);
    btn->SetText(name);
    button.insert(std::make_pair(name,btn));
}

void Window::AddEditBox(std::string name, int X, int Y, BitmapFont font){
    EditBox *edt = new EditBox;
    edt->setPosition(x+X,y+Y);
    edt->GetTexture("editbox");
    edt->GetFont(font);
    edt->SetText("");
    editbox.insert(std::make_pair(name,edt));

}

void Window::AddText(std::string name, std::string strText, int X, int Y){
    BitmapFont *font = new BitmapFont;
    font->setFont("font01",7,9);
    font->setSize(1);
    font->setText(strText,x+X,y+Y);
    text.insert(make_pair(name,font));

}

void Window::Update(){
std::unordered_map<std::string, EditBox*>::iterator ed_it;
    for(ed_it = editbox.begin(); ed_it!=editbox.end();ed_it++){
        ed_it->second->isActive=false;
        if(ed_it->second->OnClick()){
        temp = ed_it->first;
        }
}
        if(temp!="")
        {

        editbox[temp]->SetText(Events::text);
        }
}

void Window::Draw()
{

    if(isActive)
    {
    viewport.x = dst.x=x;
    viewport.y = dst.y=y;
    viewport.w = dst.w=w;
    viewport.h = dst.h=h;
    //Form
    //SDL_RenderSetClipRect(GraphicsDevice::instance().m_render, &viewport);
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,177,175,175,255);
    SDL_RenderFillRect(GraphicsDevice::instance().m_render, &dst);
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,211,211,211,255);
    SDL_RenderDrawLine(GraphicsDevice::instance().m_render,x,y,x+w,y);
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,201,201,201,255);
    SDL_RenderDrawLine(GraphicsDevice::instance().m_render,x,y,x,y+h);
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,61,61,61,255);
    SDL_RenderDrawLine(GraphicsDevice::instance().m_render,x,y+h,x+w,y+h);
    SDL_SetRenderDrawColor(GraphicsDevice::instance().m_render,61,61,61,255);
    SDL_RenderDrawLine(GraphicsDevice::instance().m_render,x+w,y,x+w,y+h);

    std::unordered_map<std::string, Button*>::iterator bt_it;
    for(bt_it = button.begin(); bt_it!=button.end();bt_it++)
        bt_it->second->Draw();

    std::unordered_map<std::string, EditBox*>::iterator ed_it;
    for(ed_it = editbox.begin(); ed_it!=editbox.end();ed_it++)
        ed_it->second->Draw();

    std::unordered_map<std::string, BitmapFont*>::iterator txt_it;
    for(txt_it = text.begin(); txt_it!=text.end();txt_it++)
        txt_it->second->draw();
    }
}
