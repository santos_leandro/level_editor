#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include <string>


class BitmapFont{
public:
        BitmapFont();
        ~BitmapFont();
        void setFont(std::string textureId, int fontWidth, int fontHeight);
        void setText(std::string text, int x_pos, int y_pos);
        std::string getText(){return text_;}
        void setSize(int Size){fontSize = Size;}
        void draw();

private:
        int fontSize;
        int x,y;
        int w, h;
        int col;
        std::string text_;
        std::string textureID;


};

#endif // FONT_H_INCLUDED
