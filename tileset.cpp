#include "tileset.h"

TileSet::TileSet(){
y=35; x=850;
idx=0; idy=0;
tileID=0;
}

TileSet::~TileSet(){
}


void TileSet::getTexture(std::string textureID, int offset){
    this->offset = offset;
    this->textureID=textureID;
    SDL_QueryTexture(TextureManager::instance().getTexture(textureID),NULL,NULL,&w,&h);
    col = w/(tilew+offset);
    row = h/tileh;
}

void TileSet::update(){
  SDL_GetMouseState(&mx, &my);
   if((mx>x)&&(mx<x+w)&&(my>y)&&(my<y+h))
   if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)){
        idx = (mx-x)/(tilew+offset);
        idy = (my-y)/(tileh+offset);
        tileID = idx+(idy*col);

   }
}

void TileSet::draw(){
view_texture.w=w;
view_texture.h=h;
view_texture.x=x;
view_texture.y=y;
SDL_RenderCopy(GraphicsDevice::instance().m_render,TextureManager::instance().getTexture(textureID),NULL,&view_texture);

}
