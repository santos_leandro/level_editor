#include "menu.h"
#include "events.h"


Menu::Menu(){
}

Menu::~Menu(){
}

void Menu::Init(){
//Debug text
    txt_debug.setFont("font01",7,9);
    txt_debug.setSize(1);

//Base font setup
    font.setFont("font01",7,9);
    font.setSize(1);

    btn_new.GetTexture("button");
    btn_new.setPosition(0,0);
    btn_new.GetFont(font);
    btn_new.SetText(" New");

    btn_open.GetTexture("button");
    btn_open.setPosition(64,0);
    btn_open.GetFont(font);
    btn_open.SetText(" Open");

    btn_save.GetTexture("button");
    btn_save.setPosition(128,0);
    btn_save.GetFont(font);
    btn_save.SetText(" Save");

    btn_about.GetTexture("button");
    btn_about.setPosition(192,0);
    btn_about.GetFont(font);
    btn_about.SetText(" About");

//About window setup
    win_about.CreateWindow(197,22,200,100);
    win_about.AddButton("Ok",70,50, font);
    win_about.AddText("about","Tilemap Level Editor 0.01\n\nLeandro Santos c 2017",10,10);

//New window setup
    win_new.CreateWindow(5,22,250,210);
    win_new.AddText("title"   ,"Create new tilemap",5,5);
    win_new.AddText("tilew"   ,"Tile Width" ,5,20);
    win_new.AddText("tileh"   ,"Tile Height",5,40);
    win_new.AddText("width"   ,"Map Width"  ,5,60);
    win_new.AddText("height"  ,"Map Height" ,5,80);
    win_new.AddText("tileset" ,"TileSet"    ,5,100);
    win_new.AddText("offset"  ,"Offset"     ,5,120);
    win_new.AddText("Filename","Filename"   ,5,140);

    win_new.AddEditBox("tilew"    ,100,20,font);
    win_new.AddEditBox("tileh"    ,100,40,font);
    win_new.AddEditBox("width"    ,100,60,font);
    win_new.AddEditBox("height"   ,100,80,font);
    win_new.AddEditBox("tileatlas",100,100,font);
    win_new.AddEditBox("offset"   ,100,120,font);
    win_new.AddEditBox("filename" ,100,140,font);

    win_new.AddButton("ok",5,170,font);
    win_new.AddButton("cancel",85,170,font);

//Open window setup
    win_open.CreateWindow(69,22,250,85);
    win_open.AddText("title","Open File",5,5);
    win_open.AddText("filename","Filename",5,20);
    win_open.AddEditBox("filename",100,20,font);
    win_open.AddButton("ok",5,45,font);
    win_open.AddButton("cancel",85,45,font);

//Save window setup
    win_save.CreateWindow(133,22,250,85);
    win_save.AddText("title","Save File",5,5);
    win_save.AddText("filename","Filename",5,20);
    win_save.AddEditBox("filename",100,20,font);
    win_save.AddButton("ok",5,45,font);
    win_save.AddButton("cancel",85,45,font);


}


void Menu::Update(){
    //New Window Actions
    if(btn_new.OnClick())
        win_new.isActive=true;

    if(win_new.isActive){
        SDL_StartTextInput();
        win_new.Update();
        if(win_new.button["cancel"]->OnClick())
            win_new.isActive=false;
    }

    //Open Window Actions
    if(btn_open.OnClick())
        win_open.isActive=true;

    if(win_open.isActive){
        SDL_StartTextInput();
        win_open.Update();
        if(win_open.button["cancel"]->OnClick()){
            SDL_Delay(600);
            win_open.isActive=false;
        }
    }

    //Save Window Actions
    if(btn_save.OnClick())
        win_save.isActive=true;

    if(win_save.isActive){
        SDL_StartTextInput();
        win_save.Update();
        if(win_save.button["cancel"]->OnClick()){
            SDL_Delay(600);
            //win_save.isActive=false;
        }
    }

    //About Window
    if(btn_about.OnClick())
    {
        win_about.isActive=true;
        txt_debug.setText("btn about",500,700);
    }
    if((win_about.isActive)&&win_about.button["Ok"]->OnClick())
    {
        win_about.isActive=false;
        txt_debug.setText("btn closed",500,700);
    }

if(!win_new.isActive&&!win_open.isActive&&!win_save.isActive)
    SDL_StopTextInput();
}

void Menu::Draw(){
    btn_new.Draw();
    btn_open.Draw();
    btn_save.Draw();
    btn_about.Draw();
    txt_debug.draw();
    win_about.Draw();
    win_new.Draw();
    win_open.Draw();
    win_save.Draw();
}
