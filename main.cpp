/*  2d Game Engine
*
*   started: 09/11/2017
*   latmod:  09/31/2017
*/



#include <iostream>
#include <memory>
#include <vector>
#include "init.h"
#include "graphics.h"
#include "texture.h"
#include "app.h"

using namespace std;




int main()
{
    App App;

    App.Init();

    while(App.Event.getState()){
    App.Update();
    App.Draw();
    }



    TextureManager::instance().unloadAll();

    GraphicsDevice::instance().quit();

    SDL_Quit();

    cout << "End of program!" << endl;
    return 0;
}
